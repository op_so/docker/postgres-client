<!-- vale off -->
# Docker Postgres client
<!-- vale on -->

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=for-the-badge)](LICENSE)
[![Pipeline Status](https://img.shields.io/gitlab/pipeline-status/op_so/docker/alpine-task?style=for-the-badge)](https://gitlab.com/op_so/docker/postgres-client/pipelines)

A [`Postgres` client](https://www.postgresql.org/docs/current/app-psql.html) Docker image:

* **lightweight** image based on Alpine Linux only 6 MB,
* `multiarch` with support of **amd64** and **arm64**,
* **non-root** container user,
* **automatically** updated by comparing software bill of materials (`SBOM`) changes,
* image **signed** with [Cosign](https://github.com/sigstore/cosign),
* a **software bill of materials (`SBOM`) attestation** added using [`Syft`](https://github.com/anchore/syft),
* available on **Docker Hub** and **Quay.io**.

[![GitLab](https://shields.io/badge/Gitlab-554488?logo=gitlab&style=for-the-badge)](https://gitlab.com/op_so/docker/postgres-client) The main repository.

[![Docker Hub](https://shields.io/badge/dockerhub-1D63ED?logo=docker&logoColor=white&style=for-the-badge)](https://hub.docker.com/r/jfxs/postgres-client) The Docker Hub registry.

[![Quay.io](https://shields.io/badge/quay.io-E5141F?logo=redhat&logoColor=white&style=for-the-badge)](https://quay.io/repository/ifxs/postgres-client) The Quay.io registry.

## Running task

```shell
docker run -t --rm jfxs/postgres-client task --version
```

or

```shell
docker run -t --rm quay.io/ifxs/postgres-client task --version
```

## Built with

Docker latest tag is [--VERSION--](https://gitlab.com/op_so/docker/postgres-client/-/blob/main/Dockerfile) and contains:

--SBOM-TABLE--

Details are updated on [`Dockerhub` Overview page](https://hub.docker.com/r/jfxs/postgres-client) when an image is published.

## Versioning

Docker tag definition:

* the `Postgres` client `psql` version used,
* a dash
* an increment to differentiate build with the same version starting at 001

```text
<psql_version>-<increment>
```

<!-- vale off -->
Example: 16.3.0-001
<!-- vale on -->

## Signature and attestation

[Cosign](https://github.com/sigstore/cosign) public key:

```shell
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEa3yV6+yd/l4zh/tfT6Tx+zn0dhy3
BhFqSad1norLeKSCN2MILv4fZ9GA6ODOlJOw+7vzUvzZVr9IXnxEdjoWJw==
-----END PUBLIC KEY-----
```

The public key is also available online: <https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub>.

To verify an image:

```shell
cosign verify --key cosign.pub $IMAGE_URI
```

To verify and get the SBOM attestation:

```shell
cosign verify-attestation --key cosign.pub --type spdxjson $IMAGE_URI | jq '.payload | @base64d | fromjson | .predicate'
```

## Authors

<!-- vale off -->
* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)
<!-- vale on -->

## License

<!-- vale off -->
This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
<!-- vale on -->
