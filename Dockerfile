# hadolint ignore=DL3007
FROM alpine:latest

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF="DEV"

ENV container=docker
ENV HOME=/home

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="postgres-client" \
    org.opencontainers.image.description="A lightweight automatically updated alpine postgres client only image" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.version="${VERSION}" \
    org.opencontainers.image.url="https://hub.docker.com/r/jfxs/postgres-client" \
    org.opencontainers.image.source="https://gitlab.com/op_so/docker/postgres-client" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

COPY files/uid-entrypoint.sh /usr/local/bin/

# hadolint ignore=DL3018
RUN apk --no-cache add ca-certificates postgresql16-client \
    && chmod 755 /usr/local/bin/uid-entrypoint.sh \
    && mkdir -p /home \
    && chgrp -R 0 /home \
    && chmod -R g=u /etc/passwd /home

USER 10010

ENTRYPOINT ["uid-entrypoint.sh"]
